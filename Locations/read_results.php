<?php

$json = file_get_contents('place2file.json');
$records = json_decode($json, true);

$results = [];
foreach ($records as $placeName => $fileName) {
	$path = 'geocoding/' . $fileName;
	if (is_file($path)) {
		$json = file_get_contents($path);
		$geo = json_decode($json, true);
		if ($geo['results'][0]['address_components'][0]['long_name'] !== 'Thailand') {
			$result = [];
			$result['location'] = $geo['results'][0]['geometry']['location'];
			$result['formatted_address'] = $geo['results'][0]['formatted_address'];
			$result['place_id'] = $geo['results'][0]['place_id'];
			$results[$placeName] = $result;
		}
	}
}

file_put_contents('place2locations.json', json_encode($results));