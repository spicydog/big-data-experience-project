<?php

$geocodingURL = 'https://maps.googleapis.com/maps/api/geocode/json?address=%s&components=country:TH&key=';

$raw = file_get_contents('placeNames.txt');
$placeNames = explode("\n", $raw);
$log = [];
$log['save'] = 0;
$log['fail'] = 0;
$log['skip'] = 0;
$n = count($placeNames);
$i = 0;
foreach ($placeNames as $placeName) {
	$i++;
	$filename = 'geocoding/' . str_replace('/', '-', $placeName) . '.json';
	echo "requesting: $placeName ($i/$n)\n";
	if (!is_file($filename)) {
		$url = sprintf($geocodingURL, urlencode($placeName));
		$json = file_get_contents($url);

		$data = json_decode($json, true);
		if (isset($data['status']) && $data['status'] === 'OK') {
			echo "requested success\n";
			file_put_contents($filename, $json);
			echo "saved\n";
			$log['save']++;
		} else {
			echo "requested fail\n";
			$log['fail']++;
		}
	} else {
		echo "skip\n";
		$log['skip']++;
	}
}
print_r($log);