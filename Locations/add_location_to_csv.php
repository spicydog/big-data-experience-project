<?php

date_default_timezone_set('Asia/Bangkok');

$raw = file_get_contents('../Raw Data/ATM.csv');
$lines = explode("\n", $raw);

$json = file_get_contents('place2locations.json');
$place2locations = json_decode($json ,true);

$headers = str_getcsv($lines[0]);
unset($lines[0]);

$headers[7] = 'Latitude';
$headers[8] = 'Longitude';
$headers[9] = 'Time';

$datetime = new DateTime();

$fp = fopen('new-atm.csv', 'w');
fputcsv($fp, $headers);

$records = [];
foreach ($lines as $line) {
	$fields = str_getcsv($line);
	foreach ($fields as $key => $value) {
		$fields[$key] = trim($value);
		if ($key === 0) {
			$fields[$key] = str_replace(',', '', $fields[$key]);
		}

		if ($key === 2) { // location
			$fields[7] = '';
			$fields[8] = '';	
			if (isset($place2locations[$fields[$key]])) {
				$location = $place2locations[$fields[$key]]['location'];
				$fields[7] = $location['lat'];
				$fields[8] = $location['lng'];	
			}
		}

		if ($key === 5) { // time
			$fields[9] = strtotime($fields[$key]);
			$fields[5] = $datetime->setTimestamp($fields[9])->format('d/m/Y H:i:s');
		}
	}
	echo $fields[0] . "\n";
	fputcsv($fp, $fields);

}

fclose($fp);