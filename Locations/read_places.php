<?php

$raw = file_get_contents('../Raw Data/ATM.csv');
$lines = explode("\n", $raw);

$headers = str_getcsv($lines[0]);
unset($lines[0]);

$places = [];
foreach ($lines as $line) {
	$records = [];
	$fields = str_getcsv($line);
	if (isset($fields[2])) {
		$placeName = $fields[2];
		$placeName = preg_replace('/\(\d+\)/', '', $placeName);
		$placeName = preg_replace('/เครื่อง \d+\)/', '', $placeName);
		$placeName = trim($placeName);
		if (mb_strlen($placeName) > 0) {
			$places[$placeName] = true;
		}
	}
}
$places = array_keys($places);

file_put_contents('placeNames.txt', implode("\n", $places));