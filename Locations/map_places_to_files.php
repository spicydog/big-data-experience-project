<?php

$raw = file_get_contents('../Raw Data/ATM.csv');
$lines = explode("\n", $raw);

$headers = str_getcsv($lines[0]);
unset($lines[0]);

$records = [];
foreach ($lines as $line) {
	$fields = str_getcsv($line);
	if (isset($fields[2])) {
		$placeName = $fields[2];
		$fileName = $placeName;
		$fileName = preg_replace('/\(\d+\)/', '', $fileName);
		$fileName = preg_replace('/เครื่อง \d+\)/', '', $fileName);
		$fileName = trim($fileName);
		$fileName = str_replace('/', '-', $fileName) . '.json';
		if (mb_strlen($fileName) > 0) {
			$records[$placeName] = $fileName;
		}
	}
}

file_put_contents('place2file.json', json_encode($records));